defmodule Mobilizon.Mailer do
  @moduledoc """
  Mailer
  """
  use Bamboo.Mailer, otp_app: :mobilizon
end
